import { RootState } from "@/store";
import { createSelector } from "reselect";

export const selectAuth = (state: RootState) => state.auth;

export const selectAuthUser = createSelector(
  [selectAuth],
  (auth) => auth.user
);

export const selectAuthLoading = createSelector(
  [selectAuth],
  (auth) => auth.loading
);

export const selectAuthRememberMe = createSelector(
  [selectAuth],
  (auth) => auth.rememberMe
);

export const selectAuthErrors = createSelector(
  [selectAuth],
  (auth) => auth.errors
);

export const selectAuthToken = createSelector(
  [selectAuth],
  (auth) => auth.jwt
);
