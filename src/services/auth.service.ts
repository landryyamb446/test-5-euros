import { TCredentials } from "@/types";
import { apiCall } from "@/utils/api-call";

export class AuthService {
  static authenticateAsync ( credentials: TCredentials ) {
      const {login:email,password} = credentials;
      return apiCall.post('/auth', {email,password}) 
  }
}
