export type TThemeColors = {
    [key: string]: TThemeColors | string;
};

export type TAuthUser = {
    id: string;
    email: string;
    firstname: string;
    lastname: string;
    roles: string;
};

export type TCredentials = {
    login: string;
    password: string;
    rememberMe: boolean;
}