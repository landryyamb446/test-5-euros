import Swal from "sweetalert2";

export const Toast = Swal.mixin({
  toast: true,
  position: "top",
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  padding: "0.5rem",
  didOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

export const convert_Date_to_inputDateFormate = (date: Date) => {
  let year = `${date.getFullYear()}`;
  let month = `${date.getMonth() + 1}`;
  month = month.padStart(2, "0");
  let day = `${date.getDate()}`;
  day = day.padStart(2, "0");

  return `${year}-${month}-${day}`;
};
