import { apiCall } from "./api-call";


export const setAuthToken = (token: string | null) => {
  if (token) {
    apiCall.defaults.headers.common["Authorization"] = `${token}`;
  } else {
    delete apiCall.defaults.headers.common["Authorization"];
  }
};
