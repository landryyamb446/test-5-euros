// import original module declarations
import "styled-components";

import { TThemeColors } from "../utils/convertThemeColorsToRootColors";

// and extend them!
declare module "styled-components" {
  export interface DefaultTheme {
    colors: TThemeColors;
  }
}
