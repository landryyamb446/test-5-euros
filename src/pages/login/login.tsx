import { Button, IconButton } from '@mui/material';
import { ChevronLeftRounded, CheckCircleRounded, LocationOnOutlined } from '@mui/icons-material';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';
import avatar from "@/assets/images/bag.png";
import React from 'react';
import "./login.scss";


export const Login: React.FC = () => {
    return (
        <div className="main-container">
            <div className="top-part">
                <IconButton className="icon-back"  aria-label="delete">
                    <ChevronLeftRounded />
                </IconButton>
                <img src={avatar} className="avatar" alt="" />
                <span className="avatar-name">Vincent V.</span>
            </div>
            <div className="rating-part">
                <div className="rate-row">
                    <CheckCircleRounded color='success' />
                    <Typography component="legend">Profil vérifié</Typography>
                </div>
                <div className="rate-row">
                    <LocationOnOutlined />
                    <Typography component="legend">Paris</Typography>
                </div>
                <div className="rate-row">
                    <Rating name="read-only" value={5} readOnly />
                    <Typography sx={{fontWeight:'bold'}} component="legend">5.0</Typography>
                </div>
            </div>
            <div className="option-part">
                <div className="card-yel">
                <Typography sx={{fontWeight:'bold', color:'#f06f7d', fontSize:'25px'}}  component="legend">26</Typography>
                <Typography  component="small">avis</Typography>
                <Typography sx={{fontWeight:'normal', color:'#f06f7d', marginTop:'10px'}}  component="legend">Voir les avis</Typography>
                </div>
                <div className="card-yel">
                    <img src={avatar} className="card-img" alt="" />
                    <Typography  component="legend">Héroïque</Typography>
                </div>
                <div className="card-yel">
                    <Typography sx={{fontWeight:'bold', color:'#f06f7d', fontSize:'25px'}}  component="legend">30</Typography>
                    <Typography  component="small">missions</Typography>
                    <Typography  component="small">accomplies</Typography>
                </div>
            </div>
            <div className="description">
                <p>
                    Je vous propose mon aide pour réaliser des <br/>
                    services de bricolage. Je suis disponible le week- <br/>
                    end parfois en soirée. N'hésitez pas à me <br/>
                    contacter pour plus d'informations. 
                </p>
            </div>
            <div className="competences-part">
                <Typography className="title"  component="legend">Compétences</Typography>
                <div className="card-yel">
                    <img src={avatar} className="competence-img" alt="" />
                    <Typography className="competence-title"  component="legend">Réparation</Typography>
                </div>
                <div className="card-yel">
                    <img src={avatar} className="competence-img" alt="" />
                    <Typography className="competence-title"  component="legend">Montage</Typography>
                </div>
                <div className="card-yel">
                    <img src={avatar} className="competence-img" alt="" />
                    <Typography className="competence-title"  component="legend">Déménagement</Typography>
                </div>
                <div className="card-yel">
                    <img src={avatar} className="competence-img" alt="" />
                    <Typography className="competence-title"  component="legend">Peinture</Typography>
                </div>
                <div className="card-yel">
                    <img src={avatar} className="competence-img" alt="" />
                    <Typography className="competence-title"  component="legend">Jardinage</Typography>
                </div>
                <div className="card-yel">
                    <img src={avatar} className="competence-img" alt="" />
                    <Typography className="competence-title"  component="legend">Football</Typography>
                </div>
            </div>
            <div className="avis-part">
                <Typography className="title"  component="legend">Avis</Typography>
                <div className="avis-slide">
                    <div className="card-avis">
                        <div className="client">
                            <img src={avatar} alt="" />
                            <Typography className="client-name"  component="legend">Nathalie</Typography>
                        </div>
                        <Typography className="avis-type"  component="legend">MONTAGE DE MEUBLE</Typography>
                        <p> 
                            « Travail impeccable. Vincent est très <br/>
                            compétent je suis ravie du résultat !»
                        </p>
                        <span className="date"> 16 sept. 2019</span>
                    </div>
                    <div className="card-avis">
                        <div className="client">
                            <img src={avatar} alt="" />
                            <Typography className="client-name"  component="legend">Nathalie</Typography>
                        </div>
                        <Typography className="avis-type"  component="legend">MONTAGE DE MEUBLE</Typography>
                        <p> 
                            « Travail impeccable. Vincent est très <br/>
                            compétent je suis ravie du résultat !»
                        </p>
                        <span className="date"> 16 sept. 2019</span>
                    </div>
                </div>
            </div>
            <Button className='submit-btn' variant="contained">Valider votre héro</Button>
        </div>
    )
}
