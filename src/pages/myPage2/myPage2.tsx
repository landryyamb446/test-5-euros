import { Box, Card, CardContent, Typography, CardActions, Button } from '@mui/material';
import React from 'react'
const bull = (
    <Box
        component="span"
        sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
    >
        •
    </Box>
);

export const MyPage2: React.FC = () => {
    return (
        <Card sx={{ minWidth: 275 }}>
            <CardContent>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                    Welcome to
                </Typography>
                <Typography variant="h5" component="div">
                    MY - 2 - {bull}PAGE
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small"> Page 1 </Button>
                <Button size="small"> Login </Button>
            </CardActions>
        </Card>
    )
}
