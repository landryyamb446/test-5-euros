import { TAuthUser } from "@/types";
import { JwtPayload } from "jwt-decode";

export interface IAPIResponseInterface {
  data: any | null;
  error: boolean;
  message: string;
}

export interface IAuthJwtPayload extends JwtPayload {
  data: TAuthUser;
}
