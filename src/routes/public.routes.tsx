import { lazyImport } from "../utils/lazyImport";
import { routePaths } from "./routesPath";


const { Login } = lazyImport(
  () => import("@/pages/login/login"),
  "Login"
);

export const publicRoutes = [
  {
    path: routePaths.login + "/*",
    element: <Login />,
  },
];
