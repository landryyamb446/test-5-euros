
import { Navigate } from "react-router-dom";
import { lazyImport } from "../utils/lazyImport";
import { routePaths } from "./routesPath";

const { MyPage } = lazyImport(
  () => import("@/pages/myPage/myPage"),
  "MyPage"
);
const { MyPage2 } = lazyImport(
  () => import("@/pages/myPage2/myPage2"),
  "MyPage2"
);


export const protectedRoutes = [
  {
    path: routePaths.page1,
    element: <MyPage />,
  },
  {
    path: routePaths.page2,
    element: <MyPage2 />,
  },
  { path: "*", element: <Navigate to="/page1" /> }
];
