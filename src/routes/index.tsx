import {
  selectAuthToken,
} from "@/store/reducers/auth/auth.selector";
// import { setAuthToken } from "@/utils/setAuthToken";
import { connect, ConnectedProps } from "react-redux";
import { Navigate, useRoutes } from "react-router-dom";
import { createStructuredSelector } from "reselect";

// import { protectedRoutes } from "./protected.routes";
import { publicRoutes } from "./public.routes";

type PropsFromRedux = ConnectedProps<typeof connector>;

type AppRoutesProps = PropsFromRedux & {
  userToken: string;
};

const AppRoutes: React.FC<AppRoutesProps> = ({ userToken }) => {
  // let userConnected = localStorage.getItem("userConnected");
  // const auth = !userConnected;
  // setAuthToken(userToken);
  //console.log(userConnected);

  const commonRoutes = [{ path: "*", element: <Navigate to="/login" /> }];

  const routes = publicRoutes; // auth ? protectedRoutes : publicRoutes;

  const element = useRoutes([...routes, ...commonRoutes]);

  return <>{element}</>;
};

const mapStateToProps = createStructuredSelector({
  userToken: selectAuthToken,
});

const connector = connect(mapStateToProps);
export default connector(AppRoutes);
