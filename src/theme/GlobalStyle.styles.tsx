import { createGlobalStyle } from "styled-components";
import convertThemeColorsToRootColors from "../utils/convertThemeColorsToRootColors";

const GlobalStyle = createGlobalStyle`
    *, ::after, ::before {
        box-sizing: border-box;
        font-family: 'Open Sans', sans-serif;
        padding: 0;
        margin: 0;
    }

    :root {
        ${({ theme }) => {
          return convertThemeColorsToRootColors(theme.colors);
        }}
    }
`;

export default GlobalStyle;
