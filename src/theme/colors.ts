import { TThemeColors } from "@/types";

export const colors: TThemeColors = {
  brand: {
    primary: "#8A93FF",
    secondary: "#303448",
  },
  ui: {
    primary: "#5865FF",
  },
  bg: {
    primary: "#FFFFFF",
    secondary: "#F4F5FF",
    tertiary: "#F7F8FF",
  },
  text: {
    primary: "#303448",
    secondary: "rgba(48, 52, 72, 0.67)",
    muted: "#A1A8BC",
    error: "#D0421B",
    success: "#138000",
  },
};
